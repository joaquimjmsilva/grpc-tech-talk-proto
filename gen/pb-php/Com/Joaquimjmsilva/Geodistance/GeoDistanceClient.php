<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Com\Joaquimjmsilva\Geodistance;

/**
 */
class GeoDistanceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Com\Joaquimjmsilva\Geodistance\GeoPointConverterRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ConvertDMStoDD(\Com\Joaquimjmsilva\Geodistance\GeoPointConverterRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.joaquimjmsilva.geodistance.GeoDistance/ConvertDMStoDD',
        $argument,
        ['\Com\Joaquimjmsilva\Geodistance\GeoPointConverterResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Joaquimjmsilva\Geodistance\CalculateLinearDistanceRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CalculateDistance(\Com\Joaquimjmsilva\Geodistance\CalculateLinearDistanceRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.joaquimjmsilva.geodistance.GeoDistance/CalculateDistance',
        $argument,
        ['\Com\Joaquimjmsilva\Geodistance\CalculateLinearDistanceResponse', 'decode'],
        $metadata, $options);
    }

}
