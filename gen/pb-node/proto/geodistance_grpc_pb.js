// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var proto_geodistance_pb = require('../proto/geodistance_pb.js');

function serialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceRequest(arg) {
  if (!(arg instanceof proto_geodistance_pb.CalculateLinearDistanceRequest)) {
    throw new Error('Expected argument of type com.joaquimjmsilva.geodistance.CalculateLinearDistanceRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceRequest(buffer_arg) {
  return proto_geodistance_pb.CalculateLinearDistanceRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceResponse(arg) {
  if (!(arg instanceof proto_geodistance_pb.CalculateLinearDistanceResponse)) {
    throw new Error('Expected argument of type com.joaquimjmsilva.geodistance.CalculateLinearDistanceResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceResponse(buffer_arg) {
  return proto_geodistance_pb.CalculateLinearDistanceResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_com_joaquimjmsilva_geodistance_GeoPointConverterRequest(arg) {
  if (!(arg instanceof proto_geodistance_pb.GeoPointConverterRequest)) {
    throw new Error('Expected argument of type com.joaquimjmsilva.geodistance.GeoPointConverterRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_com_joaquimjmsilva_geodistance_GeoPointConverterRequest(buffer_arg) {
  return proto_geodistance_pb.GeoPointConverterRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_com_joaquimjmsilva_geodistance_GeoPointConverterResponse(arg) {
  if (!(arg instanceof proto_geodistance_pb.GeoPointConverterResponse)) {
    throw new Error('Expected argument of type com.joaquimjmsilva.geodistance.GeoPointConverterResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_com_joaquimjmsilva_geodistance_GeoPointConverterResponse(buffer_arg) {
  return proto_geodistance_pb.GeoPointConverterResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var GeoDistanceService = exports.GeoDistanceService = {
  convertDMStoDD: {
    path: '/com.joaquimjmsilva.geodistance.GeoDistance/ConvertDMStoDD',
    requestStream: false,
    responseStream: false,
    requestType: proto_geodistance_pb.GeoPointConverterRequest,
    responseType: proto_geodistance_pb.GeoPointConverterResponse,
    requestSerialize: serialize_com_joaquimjmsilva_geodistance_GeoPointConverterRequest,
    requestDeserialize: deserialize_com_joaquimjmsilva_geodistance_GeoPointConverterRequest,
    responseSerialize: serialize_com_joaquimjmsilva_geodistance_GeoPointConverterResponse,
    responseDeserialize: deserialize_com_joaquimjmsilva_geodistance_GeoPointConverterResponse,
  },
  calculateDistance: {
    path: '/com.joaquimjmsilva.geodistance.GeoDistance/CalculateDistance',
    requestStream: false,
    responseStream: false,
    requestType: proto_geodistance_pb.CalculateLinearDistanceRequest,
    responseType: proto_geodistance_pb.CalculateLinearDistanceResponse,
    requestSerialize: serialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceRequest,
    requestDeserialize: deserialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceRequest,
    responseSerialize: serialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceResponse,
    responseDeserialize: deserialize_com_joaquimjmsilva_geodistance_CalculateLinearDistanceResponse,
  },
};

exports.GeoDistanceClient = grpc.makeGenericClientConstructor(GeoDistanceService);
