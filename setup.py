
from setuptools import setup

setup(
    name='grpctechtalkproto',
    version='1.0.0',
    packages=['proto'],
    package_dir={'proto': 'gen/pb_python/proto'},
    license='MIT',
    description='gRPC tech talk proto package',
    install_requires=['grpcio', 'protobuf'],
    url='https://bitbucket.org/joaquimjmsilva/grpc-tech-talk-proto',
    author='Joaquim Silva',
    author_email='joaquimjmsilva@gmail.com'
)