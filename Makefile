PROTOS  = geodistance
LANGS   = go node php python
CMDSEP  = ;

$(BASE):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@

define generate_proto_lang
    docker run -v ${CURDIR}:/defs namely/protoc-all:1.18_0 -f proto/$1.proto --lint -l $2
endef

define generate_proto
    $(foreach lang,$(LANGS),$(call generate_proto_lang,$(1),$(lang)) $(CMDSEP))
endef

.PHONY: proto
proto: ; $(info $(M) generating proto for $(LANGS)) @ ## Generate Proto
	rm -rf ./gen
	$(foreach proto,$(PROTOS),$(call generate_proto,$(proto)))
