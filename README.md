# gRPC & protobuf tech talk protocol repository
## (easy) Service communication with gRPC and protobuf

This repository contains the protocol definition for the "(easy) Service communication with gRPC and protobuf" tech talk

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for demo purposes.

### Prerequisites

To run this examples you need the following available on your machine:
* Docker Engine
* Make

## Generating stubs

```
make proto
```

### Adding support for more languages
Edit Makefile and add the desired language to LANGS list

## Built With

* [Docker](https://www.docker.com/) - Containerization
* [gRPC](https://grpc.io/) - A high-performance, open-source universal RPC framework
* [Protocol Buffers](https://developers.google.com/protocol-buffers/) - A language-neutral, platform-neutral extensible mechanism for serializing structured data

## Authors

* **Joaquim Silva** - [joaquimjmsilva](https://bitbucket.org/joaquimjmsilva/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

